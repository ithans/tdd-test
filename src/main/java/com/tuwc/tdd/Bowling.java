package com.tuwc.tdd;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author tao.dong
 */
public class Bowling {

    private List<Frame> frames;
    private Integer score;

    public Bowling(List<Frame> frames) {
        if (!checkFrames(frames)) {
            throw new IllegalArgumentException("输入参数不合法");
        }
        this.frames = frames;
    }

    private boolean checkFrames(List<Frame> frames) {
        if (null == frames || frames.isEmpty()) {
            return false;
        }
        return true;
    }

    public Bowling() {

    }

    public void calculate() {
        AtomicInteger score = new AtomicInteger(0);
        if (null != frames) {
            for (int i = 0; i < frames.size() && i < 10; i++) {

                Frame currentFrame = frames.get(i);
                Integer sum = currentFrame.getSum();

                if (currentFrame.isStrike()) {
                    Frame nextFrame = frames.get(i + 1);
                    if (nextFrame.isStrike()) {
                        score.addAndGet(frames.get(i + 2).getSum());
                    }
                    score.addAndGet(nextFrame.getSum());
                }

                if (currentFrame.isSpare()) {
                    score.addAndGet(frames.get(i + 1).getOneThrows());
                }

                score.addAndGet(sum);
            }
        }
        this.score = score.intValue();
    }

    public Integer getScore() {
        if (null != score) {
            return score;
        }
        return 0;
    }
}
