package com.tuwc.tdd;

public class Frame {
    private static final Integer DEFAULT_VALUE = 0;
    private static final Integer MIN_VALUE = 0;

    private static final Integer MAX_VALUE = 10;

    private Integer oneThrows;
    private Integer twoThrows = DEFAULT_VALUE;

    public Frame(Integer... throwsArgs) {
        if (!checkParams(throwsArgs)) {
            throw new IllegalArgumentException("参数不合法");
        }
        oneThrows = throwsArgs[0];
        if (throwsArgs.length == 2) {
            twoThrows = throwsArgs[1];
        }
    }

    private boolean checkParams(Integer[] throwsArgs) {
        if (null == throwsArgs || throwsArgs.length < 1 || throwsArgs.length > 2) {
            return false;
        }
        if (checkParamsRange(throwsArgs[0])) {
            return false;
        }
        if (throwsArgs.length == 2) {
            if (checkParamsRange(throwsArgs[1])) {
                return false;
            }
            if (throwsArgs[1] + throwsArgs[0] > MAX_VALUE) {
                return false;
            }
        }
        return true;
    }

    private boolean checkParamsRange(Integer param) {
        if (param > MAX_VALUE || param < MIN_VALUE) {
            return false;
        }
        return true;
    }

    public Integer getSum() {
        return oneThrows + twoThrows;
    }

    public boolean isSpare() {
        return getSum().equals(MAX_VALUE) && !isStrike();
    }

    public boolean isStrike() {
        return getOneThrows().equals(MAX_VALUE);
    }

    public Integer getOneThrows() {
        return oneThrows;
    }

    public Integer getTwoThrows() {
        return twoThrows;
    }
}
